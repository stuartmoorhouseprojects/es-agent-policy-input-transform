# Elastic Search Agent Policy Input Transform
This code simplifies the creation of the elasticstack_fleet_integration_policy resource from the Elastic Stack Terraform provider.

To use, copy the POST kbn:/api/fleet/package_policies call body from the "Preview API request" window into the file "api-call.json" and run the script.

## Input
```
{
  "policy_id": "<agent_policy_id>",
  "package": {
    "name": "tcp",
    "version": "1.18.1"
  },
  "name": "tcp-1",
  "description": "",
  "namespace": "default",
  "inputs": {
    "tcp-tcp": {
      "enabled": true,
      "streams": {
        "tcp.generic": {
          "enabled": true,
          "vars": {
            "listen_address": "localhost",
            "listen_port": 8080,
            "data_stream.dataset": "tcp.generic",
            "tags": [],
            "syslog_options": "field: message\n#format: auto\n#timezone: Local\n",
            "ssl": "#certificate: |\n#    -----BEGIN CERTIFICATE-----\n#    ...\n#    -----END CERTIFICATE-----\n#key: |\n#    -----BEGIN PRIVATE KEY-----\n#    ...\n#    -----END PRIVATE KEY-----\n",
            "custom": ""
          }
        }
      }
    }
  }
}
```



## Output
``` 
  input {
    input_id = "tcp-tcp"
    streams_json = jsonencode({
      "tcp.generic" : {
        "enabled" : true,
        "vars" : {
          "listen_address" : "localhost",
          "listen_port" : 8080,
          "data_stream.dataset" : "tcp.generic",
          "tags" : [],
          "syslog_options" : "field: message\n#format: auto\n#timezone: Local\n",
          "ssl" : "#certificate: |\n#    -----BEGIN CERTIFICATE-----\n#    ...\n#    -----END CERTIFICATE-----\n#key: |\n#    -----BEGIN PRIVATE KEY-----\n#    ...\n#    -----END PRIVATE KEY-----\n",
          "custom" : ""
        }
      }
    })
  }
  ```