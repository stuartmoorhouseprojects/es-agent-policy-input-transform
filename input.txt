
  input {
    input_id = "kubelet-kubernetes/metrics" 
    streams_json = jsonencode({
    "kubernetes.container": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "hosts": [
                "https://$${env.NODE_NAME}:10250"
            ],
            "period": "10s",
            "ssl.verification_mode": "none",
            "add_resource_metadata_config": "# add_resource_metadata:\n#   namespace:\n#     include_labels: [\"namespacelabel1\"]\n#   node:\n#     include_labels: [\"nodelabel2\"]\n#     include_annotations: [\"nodeannotation1\"]\n#   deployment: false\n",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.node": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "hosts": [
                "https://$${env.NODE_NAME}:10250"
            ],
            "period": "10s",
            "ssl.verification_mode": "none",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.pod": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "hosts": [
                "https://$${env.NODE_NAME}:10250"
            ],
            "period": "10s",
            "ssl.verification_mode": "none",
            "ssl.certificate_authorities": [],
            "add_resource_metadata_config": "# add_resource_metadata:\n#   namespace:\n#     include_labels: [\"namespacelabel1\"]\n#   node:\n#     include_labels: [\"nodelabel2\"]\n#     include_annotations: [\"nodeannotation1\"]\n#   deployment: false\n"
        }
    },
    "kubernetes.system": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "hosts": [
                "https://$${env.NODE_NAME}:10250"
            ],
            "period": "10s",
            "ssl.verification_mode": "none",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.volume": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "hosts": [
                "https://$${env.NODE_NAME}:10250"
            ],
            "period": "10s",
            "ssl.verification_mode": "none",
            "ssl.certificate_authorities": []
        }
    }
})      
    }


  input {
    input_id = "kube-state-metrics-kubernetes/metrics" 
    streams_json = jsonencode({
    "kubernetes.state_container": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": [],
            "add_resource_metadata_config": "# add_resource_metadata:\n#   namespace:\n#     include_labels: [\"namespacelabel1\"]\n#   node:\n#     include_labels: [\"nodelabel2\"]\n#     include_annotations: [\"nodeannotation1\"]\n#   deployment: false\n"
        }
    },
    "kubernetes.state_cronjob": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_daemonset": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_deployment": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_job": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_namespace": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_node": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_persistentvolume": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_persistentvolumeclaim": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_pod": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": [],
            "add_resource_metadata_config": "# add_resource_metadata:\n#   namespace:\n#     include_labels: [\"namespacelabel1\"]\n#   node:\n#     include_labels: [\"nodelabel2\"]\n#     include_annotations: [\"nodeannotation1\"]\n#   deployment: false\n"
        }
    },
    "kubernetes.state_replicaset": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_resourcequota": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_service": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_statefulset": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    },
    "kubernetes.state_storageclass": {
        "enabled": true,
        "vars": {
            "add_metadata": true,
            "hosts": [
                "kube-state-metrics:8080"
            ],
            "leaderelection": true,
            "period": "10s",
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "ssl.certificate_authorities": []
        }
    }
})      
    }


  input {
    input_id = "kube-apiserver-kubernetes/metrics" 
    streams_json = jsonencode({
    "kubernetes.apiserver": {
        "enabled": true,
        "vars": {
            "bearer_token_file": "/var/run/secrets/kubernetes.io/serviceaccount/token",
            "hosts": [
                "https://$${env.KUBERNETES_SERVICE_HOST}:$${env.KUBERNETES_SERVICE_PORT}"
            ],
            "leaderelection": true,
            "period": "30s",
            "ssl.certificate_authorities": [
                "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
            ]
        }
    }
})      
    }


  input {
    input_id = "kube-proxy-kubernetes/metrics" 
    streams_json = jsonencode({
    "kubernetes.proxy": {
        "enabled": true,
        "vars": {
            "hosts": [
                "localhost:10249"
            ],
            "period": "10s"
        }
    }
})      
    }


  input {
    input_id = "events-kubernetes/metrics" 
    streams_json = jsonencode({
    "kubernetes.event": {
        "enabled": true,
        "vars": {
            "period": "10s",
            "add_metadata": true,
            "skip_older": true,
            "leaderelection": true
        }
    }
})      
    }


  input {
    input_id = "container-logs-filestream" 
    streams_json = jsonencode({
    "kubernetes.container_logs": {
        "enabled": true,
        "vars": {
            "paths": [
                "/var/log/containers/*$${kubernetes.container.id}.log"
            ],
            "symlinks": true,
            "data_stream.dataset": "kubernetes.container_logs",
            "containerParserStream": "all",
            "containerParserFormat": "auto",
            "additionalParsersConfig": "# - ndjson:\n#     target: json\n#     ignore_decoding_error: true\n# - multiline:\n#     type: pattern\n#     pattern: '^\\['\n#     negate: true\n#     match: after\n",
            "custom": ""
        }
    }
})      
    }

