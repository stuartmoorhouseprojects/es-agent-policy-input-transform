import json

api_call_file = open('api-call.json')
api_call_dict = json.load(api_call_file)
api_call_dict_inputs = api_call_dict['inputs']
hcl_input_blocks = ''

for input_name in  api_call_dict_inputs:
   input_block = api_call_dict_inputs[input_name]
   if input_block["enabled"] == True:
      input_id = input_name 
      streams = json.dumps(input_block["streams"], indent=4)
      hcl_streams = "jsonencode(" + str(streams).replace('${', '$${') + ")"

      input_hcl_template  = """
  input {{
    input_id = "{format_input_id}" 
    streams_json = {format_hcl_streams}      
    }}

"""

      input_hcl_string = input_hcl_template.format(format_input_id=str(input_id), format_hcl_streams=str(hcl_streams))

      hcl_input_blocks = hcl_input_blocks + input_hcl_string

if "vars" in api_call_dict:
 vars =  json.dumps(api_call_dict["vars"], indent=4) 
 hcl_vars = "jsonencode(" + str(vars) + ")"
 vars_json_template = """
   vars_json = {format_vars_json} 
  """
 hcl_vars_json = vars_json_template.format(format_vars_json=str(hcl_vars))

 hcl_input_blocks = hcl_input_blocks + hcl_vars_json

print(hcl_input_blocks)

input_hcl_file = open('input.txt', 'w')
input_hcl_file.write(hcl_input_blocks)
